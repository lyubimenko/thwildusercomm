/**
 * The description of the module.
 */

#ifndef UserComm_H
#define UserComm_H

#include <boost/shared_ptr.hpp>
#include <alcommon/almodule.h>
#include <string>
#include <alproxies/almemoryproxy.h>
#include <althread/almutex.h>
#include <alproxies/alspeechrecognitionproxy.h>
#include <alproxies/altexttospeechproxy.h>

namespace AL
{
  class ALBroker;
}

class UserComm : public AL::ALModule
{
  public:

    UserComm(boost::shared_ptr<AL::ALBroker> broker, const std::string& name);

    virtual ~UserComm();

    virtual void init();

    void onWordRecognized(const std::string& eventName,const AL::ALValue& value,const std::string& subscriberIdentifier);

    void onFaceDetected(const std::string& eventName,const AL::ALValue& value,const std::string& subscriber);

  private:
    boost::shared_ptr<AL::ALMutex> fCallbackMutex;

};

#endif
