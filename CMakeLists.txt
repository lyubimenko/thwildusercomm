cmake_minimum_required(VERSION 2.8)

project(THWildUserComm)

find_package(qibuild)

set(_srcs
  main.cpp
  userComm.h
  userComm.cpp
)

qi_create_lib(THWildUserComm SHARED ${_srcs} SUBFOLDER naoqi)

qi_use_lib(THWildUserComm ALCOMMON ALPROXIES BOOST)
