#include "userComm.h"

#include <sstream>

#include <alvalue/alvalue.h>
#include <alcommon/alproxy.h>
#include <alcommon/albroker.h>
#include <qi/log.hpp>
#include <althread/alcriticalsection.h>
#include <alaudio/alsoundextractor.h>
#include <alproxies/altrackerproxy.h>
#include <alproxies/altexttospeechproxy.h>
#include <alproxies/alfacedetectionproxy.h>
#include <alproxies/alaudiorecorderproxy.h>
#include <alproxies/alaudioplayerproxy.h>
#include <alproxies/alspeechrecognitionproxy.h>
#include <alproxies/almotionproxy.h>
#include <alproxies/alautonomousmovesproxy.h>

//Proxies to call built-in modules (local module)
AL::ALTrackerProxy tracker("127.0.0.1", 9559);
AL::ALTextToSpeechProxy speech("127.0.0.1", 9559);
AL::ALFaceDetectionProxy face("127.0.0.1", 9559);
AL::ALAudioRecorderProxy recorder("127.0.0.1", 9559);
AL::ALAudioPlayerProxy player("127.0.0.1", 9559);
AL::ALMemoryProxy memory("127.0.0.1", 9559);
AL::ALSpeechRecognitionProxy speechRec("127.0.0.1", 9559);
AL::ALMotionProxy motion("127.0.0.1", 9559);
AL::ALAutonomousMovesProxy dance("127.0.0.1", 9559);

//'name' of last recognized/learned user
std::string lastUser = "";

//states
//IDLE - react at starting command, e.g. Hallo Annabell
//FACE - react at faces, ignore all commands
//NAME - ignore faces, ignore all commands
//COMMAND - react at  actual commands from the list of commands
enum state {IDLE, FACE, NAME, COMMAND};
state curState = IDLE;

//Stores if a face has been recognited wrong and must be learned again under a different name
bool reLearn = false;

//Lists of commands
//Commands that wake the robot, can be a whole list of commands, e.g. "Wache auf", "Guten Morgen, Annabell", etc.
std::string arrStart[] = {"Hallo Annabell"};
std::vector<std::string> wordsStart(arrStart, arrStart + sizeof(arrStart) / sizeof(arrStart[0]));
//Commands that trigger particular actions
std::string arrCommands[] = {"Hole den Ball", "Auf Wiedersehen", "Vergiss alles", "Das bin ich nicht"};
std::vector<std::string> wordsCommands(arrCommands, arrCommands + sizeof(arrCommands) / sizeof(arrCommands[0]));

//Module constructor
UserComm::UserComm(
  boost::shared_ptr<AL::ALBroker> broker,
  const std::string& name): AL::ALModule(broker, name),
    fCallbackMutex(AL::ALMutex::createALMutex())
{
  setModuleDescription("Communicate with user. Recognize the user, listen to user, talk back, interpret and issue commands.");
  functionName("onWordRecognized", getName(), "Method called when a word from the vocabulary has been recognized.");
  BIND_METHOD(UserComm::onWordRecognized);
  functionName("onFaceDetected", getName(), "Method called when a face is seen.");
  BIND_METHOD(UserComm::onFaceDetected);
}

//Module destructor
UserComm::~UserComm() {
  //unsubscribe the callback methods
  memory.unsubscribeToEvent("onWordRecognized", "UserComm");
  memory.unsubscribeToEvent("onFaceDetected", "UserComm");
}

//Module initialization
void UserComm::init() {
    //setup speech recognition (input) - language, allowed phrases, led expression
    speechRec.setVisualExpression(true);
    speechRec.setLanguage("German");
    speechRec.setWordListAsVocabulary(wordsStart);
    //setup speech (output)
    speech.setLanguage("German");
    //announce that the module has finished loading
    speech.say("Bereit");
    //turns off movement while listening (see documentation)
    dance.setExpressiveListeningEnabled(false);
    curState = IDLE;
    //qiLogInfo("module.userComm") << "Finished loading" << std::endl;
    memory.subscribeToEvent("WordRecognized", "UserComm", "onWordRecognized");
    face.setRecognitionEnabled(true);
}

void UserComm::onWordRecognized(const std::string& eventName,const AL::ALValue& value,const std::string& subscriberIdentifier) {
  //qiLogInfo("module.userComm") << "Phrase recognized " <<  value.toString() << std::endl;
  AL::ALCriticalSection section(fCallbackMutex);
  //only process the closest matching command, because they are mutually exlussive
  //for(unsigned int i = 0; i < value.getSize()/2 ; ++i){
     //value[0] - closest matching word, value[1] - confidence
     if((std::string)value[0] == "Hallo Annabell" && (float)value[1] > 0.4f && curState == IDLE){
         speech.say("Hallo, mal sehen ob ich dich schon kenne.");
         //Start face detection
         memory.subscribeToEvent("FaceDetected", "UserComm", "onFaceDetected");
         curState = FACE;
         //Start face tracker, enable head movement
         motion.setStiffnesses("HeadYaw", 1.0f);
         motion.setStiffnesses("HeadPitch", 1.0f);
         tracker.registerTarget("Face", 0.2f);
         tracker.track("Face");
         //if no face is seen in 10 seconds, stop
         qi::os::sleep(10);
         if(curState == FACE){
             curState = IDLE;
             tracker.stopTracker();
             motion.rest();
         }
     }
     if((std::string)value[0] == "Auf Wiedersehen" && (float)value[1] > 0.4f && curState == COMMAND){
         //Issue stop command to other modules
         memory.insertData("commandId", 2);
         speech.say("Auf Wiedersehen");
         //Play the audio file with the name of current user
         if(lastUser != ""){
             player.playFile(lastUser);
             lastUser = "";
         }
         //stop all
         curState = IDLE;
         tracker.stopTracker();
         motion.rest();
         //switch to start commands
         speechRec.pause(true);
         speechRec.setWordListAsVocabulary(wordsStart);
         speechRec.pause(false);
     }

     if((std::string)value[0] == "Hole den Ball" && (float)value[1] > 0.4f && curState == COMMAND){
         tracker.stopTracker();
         qi::os::sleep(1);
         //Issue stop command to other modules
         memory.insertData("commandId", 2);
         //Issue navigate command
         memory.insertData("commandId", 1);
     }

     if((std::string)value[0] == "Das bin ich nicht" && (float)value[1] > 0.4f && curState == COMMAND){
         //Issue stop command to other modules
         memory.insertData("commandId", 2);
         //Get ready to learn user anew, even if already "known"
         curState = FACE;
         memory.subscribeToEvent("FaceDetected", "UserComm", "onFaceDetected");
         reLearn = true;
     }

     if((std::string)value[0] == "Vergiss alles" && (float)value[1] > 0.4f && curState == COMMAND){
         //Issue stop command to other modules
         memory.insertData("commandId", 2);
         speech.say("Ich habe alle Gesichte vergessen.");
         //clear face database
         face.clearDatabase();
         //stop all
         curState = IDLE;
         tracker.stopTracker();
         motion.rest();
         //switch to start commands
         speechRec.pause(true);
         speechRec.setWordListAsVocabulary(wordsStart);
         speechRec.pause(false);
     }
  //}
}

void UserComm::onFaceDetected(const std::string& eventName,const AL::ALValue& value,const std::string& subscriber){
    if(curState == FACE){
        curState = NAME;
        //qiLogInfo("module.userComm") << "Face detected" << std::endl;
        //memory.unsubscribeToEvent("onWordRecognized", "UserComm");
        //'label' associated with the face (see documentation)
        std::string name = (std::string)value[1][0][1][2];
        //if not known or must be learned again
        if(name == "" || reLearn){
            speech.say("Ich kenne dich nicht");
            //count existing known faces
            AL::ALValue knownFaces = face.getLearnedFacesList();
            int next = knownFaces.getSize();
            //convert to string
            std::stringstream ss;
            ss << next;
            std::string intStr = ss.str();
            //build path for sound file with the name (next number, if 3 faces already known, 3.wav)
            std::string path = "/home/nao/names/" + intStr + ".wav";
            //qiLogInfo("module.userComm") << "Sound path: " << path << std::endl;
            //only record from front
            AL::ALValue channels = AL::ALValue::array(0, 0, 1, 0);
            speech.say("Sage mir bitte deinen Namen");
            //start recording to sound file
            recorder.startMicrophonesRecording(path, "wav", 16000, channels);
            //save face with label = path to sound file
            face.learnFace(path);
            qi::os::sleep(1);
            recorder.stopMicrophonesRecording();
            qi::os::sleep(0.2f);
            //store path to file for communication, e.g. for "Auf Wiedersehen, <name>"
            lastUser = path;
            //greet learned user
            speech.say("Hallo");
            //play file associated with face, ideally the person's name
            player.playFile(path);
            if(reLearn) reLearn = false;
        }else{
            //if already known, just greet user
            speech.say("Hallo");
            player.playFile(name);
            lastUser = name;
        }
        speech.say("Was kann ich für dich tun?");
        //stop reacting at faces until next start/command to relearn
        memory.unsubscribeToEvent("onFaceDetected", "UserComm");
        //switch to actual command phrases
        speechRec.pause(true);
        speechRec.setWordListAsVocabulary(wordsCommands);
        speechRec.pause(false);
        curState = COMMAND;
    }
}
